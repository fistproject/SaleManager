package me.project.customermanagement.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import me.project.customermanagement.dao.impl.CustomerDAOImpl;
import me.project.customermanagement.entities.Customer;

@Service
@Transactional
public class CustomerService {

	  @Autowired
	  private CustomerDAOImpl customerDAO;
	  
	  public List<Customer> findAll() {
		  return customerDAO.findAll();
	  }
	  
	  public Customer findById(final int id) {
		  return customerDAO.findById(id);
	  }
	  
	  public List<Customer> findByEmail(final String email) {
		  return customerDAO.findByEmail(email);
	  }
	  public List<Customer> findByEmailNotHaveId(final String email, final int id) {
		  return customerDAO.findByEmailNotHaveId(email, id);
	  }
	  public List<Customer> findByPhone(final String phone) {
		  return customerDAO.findByPhone(phone);
	  }
	  
	  public List<Customer> findByPhoneNotHaveId(final String phone, final int id){
		  return customerDAO.findByPhoneNotHaveId(phone, id);
	  }
	  public void save(final Customer customer) {
		  customerDAO.save(customer);
	  }
	  
	  public void update(final Customer customer) {
		  customerDAO.update(customer);
	  }
	  
	  public void delete(final int id) {
		  Customer customer = customerDAO.findById(id);
		  if (customer != null) {
			  customerDAO.delete(customer);
	    }
	  }
}
