package me.project.customermanagement.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import me.project.customermanagement.dao.impl.DetailReceiptDAOImpl;
import me.project.customermanagement.entities.DetailReceipt;

@Service
@Transactional
public class DetailReceiptService {

	@Autowired
	private DetailReceiptDAOImpl detailReceiptDAO;
	
	public List<DetailReceipt> findListDetailByReceiptId(final int DetailReceiptId) {
		return detailReceiptDAO.findListDetailByReceiptId(DetailReceiptId);
	}
	
	public List<DetailReceipt> findAll(){
		return detailReceiptDAO.findAll();
	}
	
	public DetailReceipt findById(final int id) {
		return detailReceiptDAO.findById(id);
	}
	
	public void save(final DetailReceipt detailReceipt) {
		detailReceiptDAO.save(detailReceipt);
	}
	
	public void update(final DetailReceipt detailReceipt) {
		detailReceiptDAO.update(detailReceipt);
	}
	public void delete(final DetailReceipt detailReceipt) {
		detailReceiptDAO.delete(detailReceipt);
	}
}
