package me.project.customermanagement.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import me.project.customermanagement.dao.impl.CategoryDAOImpl;
import me.project.customermanagement.entities.Category;

@Service
@Transactional
public class CategoryService {

	  @Autowired
	  private CategoryDAOImpl categoryDAO;
	  
	  public List<Category> findAll(){
		  List<Category> categories = categoryDAO.findAll();
		  return categories;
	  }

	  public List<Category> setSupCategoryNotNull(List<Category> categories ){
			Category ca= new Category();
			for (int i = 0; i < categories.size(); i++) {
				if (categories.get(i).getSupperCategory() == null) {
					categories.get(i).setSupperCategory(ca);
				}
			}
			return categories;
	  }
	  
	  public Category findById(final int id) {
		  return categoryDAO.findById(id);
	  }
	  public void save(final Category category) {
		  categoryDAO.save(category);
	  }
		
	  public void update(final Category category) {
		  categoryDAO.update(category);
	  }
		
	  public void delete(final Category category) {
		  categoryDAO.delete(category);
	  }
	  
	  public List<Category> findTopSupperCategory(){
		  List<Category> categories = categoryDAO.findTopSupperCategory();
			return categories;
	  }
	  public List<Category> findListSubCategoryOfCategory(Category category){
		  return categoryDAO.findListSubCategoryOfCategory(category);
	  }
	  
	//get list subCategory of category
	  public List<Category> findAllListSubCategoryOfCatgory(Category category) {
		  
		  List<Category> subCategories = category.getCategories();
		  List<Category> listCategory = findAll();
		  if (subCategories == null) {
			  return new ArrayList<>();
		  } else {
			  List<Category> subCategoriesTemp =  subCategories;
			  for (Category c : listCategory) {
				  for (Category c2 : subCategoriesTemp) {
					  if (c.getSupperCategory() == c2) {
						  subCategories.add(c);
					  }
				  }
				  subCategoriesTemp = subCategories;
			  }
			  return subCategories;
		  }
	  }
	  
	  //kiem tra supper có giá trị enable = 0? .
	  public boolean enableSuperCategory(Category category){
		  List<Category> listSuperCategory = new ArrayList<>();
		  Category tempSupperCategory = category.getSupperCategory();
		  if (tempSupperCategory == null) return true;
		  while( tempSupperCategory != null) {
			  listSuperCategory.add(tempSupperCategory);
			  tempSupperCategory = tempSupperCategory.getSupperCategory();
		  }
		  for (Category c : listSuperCategory) {
			  if (!c.isEnable()) {
				  return false;
			  }
		  }
		  return true;
	  }
		
	  
	  public List<Category> setEnableListCategory(List<Category> categories){
		  for (int i = 0; i < categories.size(); i++) {
			  if (categories.get(i).getSupperCategory() != null) {
				  if (categories.get(i).getSupperCategory().getCategory_name() != null) {
					  if (!enableSuperCategory(categories.get(i))) {
						  categories.get(i).setEnable(false);
					  }
				  }
			  }
		  }
		  return categories;
	  }
	  
}
