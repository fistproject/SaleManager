package me.project.customermanagement.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import me.project.customermanagement.dao.impl.ProductDAOImpl;
import me.project.customermanagement.entities.Product;

@Service
@Transactional
public class ProductService {

	@Autowired
	private ProductDAOImpl productDAO;
	
	public List<Product> findAll() {
		return productDAO.findAll();
	}
	
	public Product findById(final int id) {
		return productDAO.findById(id);
	}
	
	public void save(final Product product) {
		productDAO.save(product);
	}
	
	public void update(final Product product) {
		productDAO.update(product);
	}
	
	public void delete(final Product product) {
		productDAO.delete(product);
	}
}
