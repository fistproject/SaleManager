package me.project.customermanagement.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import me.project.customermanagement.dao.impl.ReceiptDAOImpl;
import me.project.customermanagement.entities.DetailReceipt;
import me.project.customermanagement.entities.Receipt;

@Service
@Transactional
public class ReceiptService {

	@Autowired
	private ReceiptDAOImpl receiptDAO;
	
	public List<Receipt> findAll(){
		return receiptDAO.findAll();
	}
	
	public Receipt findById(final int id) {
		Receipt receipt = receiptDAO.findById(id);
		receipt.getCustomer();
		return receipt;
	}
	
	
	public List<DetailReceipt> findListDetailByReceiptId(final int DetailReceiptId) {
		List<DetailReceipt> detailReceipts = receiptDAO.findListDetailByReceiptId(DetailReceiptId);
		return detailReceipts;
	}
	
	public void save(final Receipt receipt) {
		receiptDAO.save(receipt);
	}
}
