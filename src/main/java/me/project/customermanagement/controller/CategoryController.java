package me.project.customermanagement.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import me.project.customermanagement.entities.Category;
import me.project.customermanagement.entities.Product;
import me.project.customermanagement.service.CategoryService;

@Controller
public class CategoryController {

	@Autowired
	private CategoryService categoryService;
	
	@RequestMapping(value = {"/category","/category/category-list"})
	public String listCategory(Model model) {
		List<Category> categories = getListTopSuperCategory();
		categories = categoryService.setEnableListCategory(categories);
		model.addAttribute("listCategory", categories);
		return "category/category-list";
	}
	
	@RequestMapping(value = {"category/category-save"})
	public String insertCategory(Model model) {
		List<Category> categories = categoryService.findAll();
		categories = categoryService.setEnableListCategory(categories);
		model.addAttribute("categories", categories);
		model.addAttribute("category", new Category());
		return "category/category-save";
	}
	
	@RequestMapping("category/category-view/{id}")
	public String viewCategory(@PathVariable int id, Model model)throws Exception  {
		Category category = categoryService.findById(id);
		List<Category> categories = categoryService.findListSubCategoryOfCategory(category);
		
		categories = categoryService.setEnableListCategory(categories);
		if (category != null) {
			Category ca = new Category();
			if (category.getSupperCategory() == null) {
				category.setSupperCategory(ca);
			}
			
			//nếu enable của category = true thì hiển thị product 
			//nếu category disable thì không hiển thị product
//			if (categoryService.enableSuperCategory(category)) {
//				List<Product> listProduct = category.getProducts();
//				model.addAttribute("listProductOfCategory", listProduct);
//			}
			List<Product> listProduct = category.getProducts();
			model.addAttribute("listProductOfCategory", listProduct);
			model.addAttribute("listSubCategory", categories);
			model.addAttribute("category", category);
			return "category/category-view";
		}else {
			List<Category> listCategory = categoryService.findTopSupperCategory();
			listCategory = categoryService.setEnableListCategory(listCategory);
			model.addAttribute("errors", "không tồn tại category!");
			model.addAttribute("listCategory", listCategory);
			return "category/category-list";
		}
	}
	
	@RequestMapping(value = {"category/saveCategory"}, method = RequestMethod.POST)
	public Object doSaveCategory(@RequestParam("supperCategory.id") String id, @RequestParam("category_name") String cName,
			@ModelAttribute("category") @Valid  Category category, BindingResult theBindingResult,  Model model){
		if (id == "" && cName != "" ) {
			Category ca = null;
			category.setSupperCategory(ca);
		}else {
			
			if (theBindingResult.hasErrors()) {
				List<Category> categories = categoryService.findAll();
				categories = categoryService.setEnableListCategory(categories);
				model.addAttribute("categories", categories);
				return "category/category-save";
			}
			Category supperCategory = categoryService.findById(Integer.parseInt(id));
			category.setSupperCategory(supperCategory);
			
			categoryService.save(category);
			if (supperCategory != null) {
				List<Category> categories = categoryService.findListSubCategoryOfCategory(supperCategory);
				categories = categoryService.setEnableListCategory(categories);
				List<Product> listProduct = supperCategory.getProducts();
				model.addAttribute("listProductOfCategory", listProduct);
				model.addAttribute("listSubCategory", categories);
				model.addAttribute("category", supperCategory);
				return "category/category-view";
			}
		}
//		try {
//			categoryService.save(category);
//        }catch (SQLException e){
//        	 System.out.println("mamamamam"+e.getSQLState());
//        }
		categoryService.save(category);
		List<Category> categories = getListTopSuperCategory();
		categories = categoryService.setEnableListCategory(categories);
		model.addAttribute("listCategory", categories);
		return "category/category-list";
	}
	
	@RequestMapping("category/category-update/{id}")
	public String updateCategory(@PathVariable int id, Model model) {
		Category category = categoryService.findById(id);
		model.addAttribute("category", category);
		List<Category> categories = categoryService.findAll();
		categories = categoryService.setEnableListCategory(categories);
		model.addAttribute("categories", categories);
		return "category/category-update";
	}
	
	@RequestMapping("category/updateCategory")
	public String doUpdateCategory(@RequestParam("supperCategory.id") String id, @RequestParam("category_name") String cName,
			@ModelAttribute("category") @Valid Category category, BindingResult theBindingResult, Model model)throws Exception {
		
		if (id == "" && cName != "" ) {
			Category ca = null;
			category.setSupperCategory(ca);
		}else {
			
			if (theBindingResult.hasErrors()) {
				List<Category> categories = categoryService.findAll();
				categories = categoryService.setEnableListCategory(categories);
				model.addAttribute("categories", categories);
				return "category/category-update";
			}
			Category supperCategory = categoryService.findById(Integer.parseInt(id));
			category.setSupperCategory(supperCategory);
			categoryService.update(category);
			if (supperCategory != null) {
				List<Category> categories = categoryService.findListSubCategoryOfCategory(supperCategory);
				categories = categoryService.setEnableListCategory(categories);
				List<Product> listProduct = supperCategory.getProducts();
				model.addAttribute("listProductOfCategory", listProduct);
				model.addAttribute("listSubCategory", categories);
				model.addAttribute("category", supperCategory);
				return "category/category-view";
			}
		}
		categoryService.update(category);
		List<Category> categories = getListTopSuperCategory();
		categories = categoryService.setEnableListCategory(categories);
		model.addAttribute("listCategory", categories);
		return "category/category-list";
	}
	
	
	@RequestMapping("category/categoryDelete/{id}")
	public String doDelete(@PathVariable int id, Model model) {
		
		Category category = categoryService.findById(id);
		if (category != null) {
			
			List<Category> subCategories = category.getCategories();
			List<Product> products = category.getProducts();
			if (subCategories.size() != 0 || products.size() != 0) {
				model.addAttribute("errors", "Only empty categories are deleted");
			} else {
				categoryService.delete(category);
			}
			//nếu có sub thì trả về view sub
			Category supperCategory = category.getSupperCategory();
			category.setSupperCategory(supperCategory);
			if (supperCategory != null) {
				List<Category> categories = categoryService.findListSubCategoryOfCategory(supperCategory);
				categories = categoryService.setEnableListCategory(categories);
				List<Product> listProduct = supperCategory.getProducts();
				model.addAttribute("listProductOfCategory", listProduct);
				model.addAttribute("listSubCategory", categories);
				model.addAttribute("category", supperCategory);
				return "category/category-view";
			} else { 
				//nếu không có sub thì trả về list
				List<Category> categories = getListTopSuperCategory();
				categories = categoryService.setEnableListCategory(categories);
				model.addAttribute("listCategory", categories);
				return "/category/category-list";
			}
		} else {
			List<Category> categories = categoryService.findTopSupperCategory();
			categories = categoryService.setEnableListCategory(categories);
			model.addAttribute("listCategory", categories);
			model.addAttribute("errors", "Không tồn tại category!");
			return "redirect:/category/category-list";
		}
	}
	
	@RequestMapping("category/categoryEnable/{id}")
	public String doEnale(@PathVariable int id, Model model) {
		
		Category category = categoryService.findById(id);
		List<Category> allSubCategory = categoryService.findAllListSubCategoryOfCatgory(category);
		
		if (!categoryService.enableSuperCategory(category)) {//nếu super của nó là F thì nó sẽ F
			category.setEnable(false);
		}
		//nếu đang enable thì tiến hành disable
		if (category.isEnable()) {
			category.setEnable(false);
			categoryService.update(category);
			for (int i = 0; i < allSubCategory.size(); i++) {
				allSubCategory.get(i).setEnable(false);
			}
			//trả về cho trang nào
			Category supperCategory = category.getSupperCategory();
			category.setSupperCategory(supperCategory);
			if (supperCategory != null) {
				List<Category> categories = categoryService.findListSubCategoryOfCategory(supperCategory);
				categories = categoryService.setEnableListCategory(categories);
				//List<Product> listProduct = supperCategory.getProducts();
				//model.addAttribute("listProductOfCategory", listProduct);
				model.addAttribute("listSubCategory", categories);
				model.addAttribute("category", supperCategory);
				return "category/category-view";
			} else { 
				//nếu không có sub thì trả về list
				List<Category> categories = getListTopSuperCategory();
				categories = categoryService.setEnableListCategory(categories);
				model.addAttribute("listCategory", categories);
				return "/category/category-list";
			}
		} else {
			//nếu là disable thì enable nó và trả về trạng thái trước khi disable cho các sub
			//nếu supper của nó enable = 0 thì ko đc enable nó
			Category supperCategory = category.getSupperCategory();
			category.setSupperCategory(supperCategory);
			if (supperCategory != null) {
				if (categoryService.enableSuperCategory(category)) {
					category.setEnable(true);
					categoryService.update(category);
				} else {
					model.addAttribute("errorEnable", "Can not Enable Category because Super category is Disable");
				}
				
				List<Category> categories = categoryService.findListSubCategoryOfCategory(supperCategory);
				categories = categoryService.setEnableListCategory(categories);
				List<Product> listProduct = supperCategory.getProducts();
				model.addAttribute("listProductOfCategory", listProduct);
				model.addAttribute("listSubCategory", categories);
				model.addAttribute("category", supperCategory);
				return "category/category-view";
			} else {
				if (categoryService.enableSuperCategory(category)) {
					category.setEnable(true);
					categoryService.update(category);
				}
				List<Category> categories = getListTopSuperCategory();
				categories = categoryService.setEnableListCategory(categories);
				model.addAttribute("listCategory", categories);
				return "/category/category-list";
			}
		}
	}

	public List<Category> getListTopSuperCategory(){
		List<Category> categories = categoryService.findTopSupperCategory();
		Category ca= new Category();
		for (int i = 0; i < categories.size(); i++) {
			if (categories.get(i).getSupperCategory() == null) {
				categories.get(i).setSupperCategory(ca);
			} 		
		}
		return categories;
	}
	
	//nếu CATEGORY F mà supper của nó là F thì ko đc enable
	//xử lí trả về trang //supper master hay detail
	
}
