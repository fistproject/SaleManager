package me.project.customermanagement.controller;

import java.time.Instant;
import java.util.List;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import me.project.customermanagement.entities.Customer;
import me.project.customermanagement.service.CustomerService;
import me.project.customermanagement.utils.Validator;

@Controller
public class CustomerController {

	  @Autowired
	  private CustomerService customerService;

	  List<Customer> customers;
	 
	  @RequestMapping(value = {"/customer", "/customer/customer-list"})	  
	  public String listCustomer(Model model) {
		  List<Customer> customers = customerService.findAll();
		  model.addAttribute("listCustomer", customers);
		  return "customer/customer-list";
	  }

	  @RequestMapping(value = {"customer/customer-save"})
	  public String insertCustomer(Model model) {
		  model.addAttribute("customer", new Customer());
		  return "customer/customer-save";
	  }

	  @RequestMapping("customer/customer-view/{id}")
	  public String viewCustomer(@PathVariable int id, Model model)throws Exception  {
		  
		  Customer customer = customerService.findById(id);
		  if (customer.getIsValid()) {
			  model.addAttribute("customer", customer);
			  return "customer/customer-view";
		  }else {
			  model.addAttribute("errors", "không tồn tại customer!");
			  model.addAttribute("listCustomer", customerService.findAll());
			  return "redirect:customer/customer-list";
		  }
	  }

	  @RequestMapping("customer/customer-update/{id}")
	  public String updateCustomer(@PathVariable int id, Model model) {
		  Customer customer = customerService.findById(id);
		  String date = Validator.convertLongToString(customer.getDob());
		  model.addAttribute("customer", customer);
		  model.addAttribute("dob", date);
		  return "customer/customer-update";
	  }

	  @RequestMapping(value="customer/saveCustomer", method = RequestMethod.POST)
	  public Object doSaveCustomer(@RequestParam("birthday") String dob, @ModelAttribute("customer") @Valid  Customer customer, BindingResult theBindingResult,  Model model)throws Exception {
		  if (theBindingResult.hasErrors()) {
			  return "customer/customer-save";
		  }
		  //check truoc khi insert
		  //check phone
		  if (!Validator.checkPhone(customer.getPhone())) {
			  model.addAttribute("errorPhone", "Số điện thoại không hợp lệ!");
			  model.addAttribute("customer", customer);
			  model.addAttribute("dob", dob);
			  return "customer/customer-save";
		  }
		  //check email
		  if (!Validator.checkEmail(customer.getEmail())) {
			  model.addAttribute("errorEmail", "Email không hợp lệ!");
			  model.addAttribute("customer", customer);
			  model.addAttribute("dob", dob);
			  return "customer/customer-save";
		  }
		  //tìm Email đã tồn tại
		  List<Customer> otherSameEmailCustomers = customerService.findByEmail(customer.getEmail());
		  if (otherSameEmailCustomers.size() != 0) {
			  model.addAttribute("errorEmail", "Email đã tồn tại!");
			  model.addAttribute("customer", customer);
			  model.addAttribute("dob", dob);
			  return "customer/customer-save";
		  }
		  //tìm phone đã tồn tại
		  List<Customer> otherSamePhoneCustomers = customerService.findByPhone(customer.getPhone());
		  if (otherSamePhoneCustomers.size() != 0) {
			  model.addAttribute("errorPhone", "Số điện thoại đã tồn tại!");
			  model.addAttribute("customer", customer);
			  model.addAttribute("dob", dob);
			  return "customer/customer-save";
		  }
		  //email, phone  hop le
		  long unix_time_birthday = Validator.convertStringToLong(dob);
		  long unix_time_today = Validator.convertStringToLong(java.time.LocalDate.now().toString());
		  if (unix_time_birthday >= unix_time_today) {
			  model.addAttribute("errorDOB", "Ngày sinh phải nhỏ hơn hôm nay!");
			  model.addAttribute("customer", customer);
			  model.addAttribute("dob", dob);
			  return "customer/customer-save";
		  }
		  customer.setDob(unix_time_birthday);
		  customerService.save(customer);
		  model.addAttribute("listCustomer", customerService.findAll());
		  return "redirect:customer/customer-list";
	  }
	  
	  @RequestMapping("customer/updateCustomer")
	  public String doUpdateCustomer(@RequestParam("birthday") String dob, @ModelAttribute("customer") @Valid Customer customer, BindingResult theBindingResult, Model model)throws Exception {
		  if (theBindingResult.hasErrors()) {
			  return "customer/customer-update";
		  }
		  //check phone có hợp lệ ko
		  if (!Validator.checkPhone(customer.getPhone())) {
			  model.addAttribute("errorPhone", "Số điện thoại không hợp lệ!");
			  model.addAttribute("customer", customer);
			  model.addAttribute("dob", dob);
			  return "customer/customer-update";	
		  }
		  
		  //check email
		  if (!Validator.checkEmail(customer.getEmail())) {
			  model.addAttribute("errorEmail", "Email không hợp lệ!");
			  model.addAttribute("customer", customer);
			  return "customer/customer-update";
		  } 
		  //tìm Email đã tồn tại
		  List<Customer> otherSameEmailCustomers = customerService.findByEmailNotHaveId(customer.getEmail(), customer.getId());
		  if (otherSameEmailCustomers.size() != 0) {
			  model.addAttribute("errorEmail", "Email đã tồn tại!");
			  model.addAttribute("customer", customer);
			  model.addAttribute("dob", dob);
			  return "customer/customer-update";
		  }
		  
		  //tìm phone đã tồn tại
		  List<Customer> otherSamePhoneCustomers = customerService.findByPhoneNotHaveId(customer.getPhone(),customer.getId());
		  if (otherSamePhoneCustomers.size() != 0) {
			  model.addAttribute("errorPhone", "Số điện thoại đã tồn tại!");
			  model.addAttribute("customer", customer);
			  model.addAttribute("dob", dob);
			  return "customer/customer-update";
		  }
		  long unix_time_birthday = Validator.convertStringToLong(dob);
		  
		  //so sanh ngay sinh voi ngay hien tai
		  long now= Instant.now().getEpochSecond();
		  if (unix_time_birthday >= now) {
			  model.addAttribute("errorDOB", "Ngày sinh phải nhỏ hơn hôm nay!");
			  model.addAttribute("customer", customer);
			  model.addAttribute("dob", dob);
			  return "customer/customer-update";
		  }
		  customer.setDob(unix_time_birthday);
		  customerService.update(customer);
		  model.addAttribute("listCustomer", customerService.findAll());
		  return "redirect:customer-list";
	  }
	  
	  @RequestMapping("customer/customerDelete/{id}")
	  public String doDeleteCustomer(@PathVariable int id, Model model){
		  //lay customer tu ID 
		  Customer customer = customerService.findById(id);
		  if (customer != null) {
			  //set IsValid cua Customer do bang false
			  //customer.setIsValid(false);
			  customerService.delete(customer.getId());
			  customers = customerService.findAll();
			  model.addAttribute("listCustomer", customers);
			  return "redirect:/customer/customer-list";
		  } else {
			  //error
			  customers = customerService.findAll();
			  model.addAttribute("listCustomer", customers);
			  model.addAttribute("errors", "không tồn tại customer!");
			  return "/customer/customer-list";
		  }
	  }
}
