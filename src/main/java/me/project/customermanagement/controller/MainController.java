package me.project.customermanagement.controller;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {
	
    @GetMapping("/")
    public String index() {
//     	Authentication auth = SecurityContextHolder.getContext().getAuthentication(); 
//      System.out.println("username: " + auth.getName()); 
//      System.out.println(auth.getAuthorities());
        return "index";
    }

    @GetMapping("/admin") 
    public String admin() {
        return "admin";
    }

    @GetMapping("/error/403")
    public String accessDenied() {
        return "/error/403";
    }
    @GetMapping("/error/400")
    public String badRequest() {
        return "/error/400";
    }
    
    @GetMapping("/error/404")
    public String notFoundPage() {
        return "/error/404";
    }

    @GetMapping("/login") 
    public String getLogin() {
        return "login";
    }

	@GetMapping("/logout")
    public String logout(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
        	new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/";
    }

}