package me.project.customermanagement.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import me.project.customermanagement.entities.DetailReceipt;
import me.project.customermanagement.entities.Product;
import me.project.customermanagement.entities.Receipt;
import me.project.customermanagement.service.DetailReceiptService;
import me.project.customermanagement.service.ProductService;
import me.project.customermanagement.service.ReceiptService;

@Controller
public class DetailReceiptController {

	@Autowired
	private DetailReceiptService detailReceiptService;
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private ReceiptService receiptService;
	
	@RequestMapping(value = {"receipt/detailReceipt-save/{id}"})
	public Object insertDetailReceipt(@PathVariable("id") int id, Model model){
		System.out.println(id);
		model.addAttribute("receipt_id", id);
		List<Product> products = productService.findAll();
		model.addAttribute("products", products);
		
		DetailReceipt detailReceipt = new DetailReceipt();
		Receipt receipt = receiptService.findById(id);
		detailReceipt.setReceipt(receipt);
		model.addAttribute("detailReceipt", detailReceipt);
		return "receipt/detailReceipt-save";
	}
	
	@RequestMapping(value = {"receipt/saveDetailReceipt"}, method = RequestMethod.POST)
	public Object doSaveDetailReceipt(@RequestParam("receiptId") String receiptId,
			@ModelAttribute("detailReceipt") @Valid  DetailReceipt detailReceipt, BindingResult theBindingResult,  Model model) throws Exception {
		
		model.addAttribute("receipt_id", receiptId);
		Receipt receipt = receiptService.findById(Integer.parseInt(receiptId));
		detailReceipt.setReceipt(receipt);
		int idProductInDetail = detailReceipt.getProduct().getId();
		Product product = productService.findById(idProductInDetail);
		if (product == null) {
			List<Product> products = productService.findAll();
			model.addAttribute("products", products);
			model.addAttribute("errorProduct", "Product name not null");
			model.addAttribute("detailReceipt", detailReceipt);
			return "receipt/detailReceipt-save";
		}
		if (theBindingResult.hasErrors()) {
			List<Product> products = productService.findAll();
			model.addAttribute("products", products);
			model.addAttribute("detailReceipt", detailReceipt);
			return "receipt/detailReceipt-save";
		}
		if (detailReceipt.getQuantity() == 0 ) {
			List<Product> products = productService.findAll();
			model.addAttribute("products", products);
			model.addAttribute("errorQuantity", "Quantity must be greater than 1");
			model.addAttribute("detailReceipt", detailReceipt);
			return "receipt/detailReceipt-save";
		}
		if (product.getQuantity() >= detailReceipt.getQuantity()) {
			//neu product do da mua roi thi khong tao moi detail ma tang so luong cua detail cu
			List<DetailReceipt> ListDetailOfReceipt = receipt.getDetailReceipts();
			
			for (int i = 0; i < ListDetailOfReceipt.size(); i++) {
				DetailReceipt detail = ListDetailOfReceipt.get(i);
				if ( idProductInDetail == detail.getProduct().getId()) {
					long quantityOlld = detail.getQuantity();
					long quantity = quantityOlld + detailReceipt.getQuantity();
					detailReceipt = receipt.getDetailReceipts().get(i);
					detailReceipt.setQuantity(quantity);
					detailReceiptService.update(detailReceipt);
					receipt = receiptService.findById(receipt.getId());
					model.addAttribute("receipt", receipt);
					return "/receipt/receipt-view";
				}
			}
			detailReceiptService.save(detailReceipt);
			receipt = receiptService.findById(receipt.getId());
			model.addAttribute("receipt", receipt);
			return "/receipt/receipt-view";
		} else {
			model.addAttribute("errorQuantity", "Quantity product not enough");
			
			List<Product> products = productService.findAll();
			model.addAttribute("products", products);
			model.addAttribute("detailReceipt", detailReceipt);
			return "/receipt/detailReceipt-save";
		}
	}
	
	@RequestMapping(value = {"receipt/detailReceipt-update/{id}"})
	public Object updateDetailReceipt(@PathVariable("id") int id, Model model){
		DetailReceipt detailReceipt = detailReceiptService.findById(id);
		model.addAttribute("receipt_id", detailReceipt.getReceipt().getId());
		model.addAttribute("detailReceipt", detailReceipt);
		return "receipt/detailReceipt-update";
	}
	
	@RequestMapping(value = {"receipt/updateDetailReceipt"}, method = RequestMethod.POST)
	public Object doUpdateDetailReceipt(@ModelAttribute("detailReceipt") @Valid DetailReceipt detailReceipt, BindingResult theBindingResult, Model model) {
		
		int idProductInDetail = detailReceipt.getProduct().getId();
		Product product = productService.findById(idProductInDetail);
		model.addAttribute("receipt_id", detailReceipt.getReceipt().getId());
		//check product có để trông hay không
		if (product == null) {
			List<Product> products = productService.findAll();
			model.addAttribute("products", products);
			model.addAttribute("errorProduct", "Product name not null");
			model.addAttribute("detailReceipt", detailReceipt);
			return "receipt/detailReceipt-save";
		}
		if (theBindingResult.hasErrors()) {
			model.addAttribute("detailReceipt", detailReceipt);
			return "receipt/detailReceipt-update";
		}
		//lay sl old trong detail
		DetailReceipt detailOld = detailReceiptService.findById(detailReceipt.getId());
		long quantityOld = detailOld.getQuantity();
		//lay sl trong product
		long quantityProduct = product.getQuantity();
		//lay sl moi trong form
		long quantityNew = detailReceipt.getQuantity();
		
		//nếu số lượng mới = 0 thì delete nó.
		if (quantityNew == 0) {
			return doDeleteDetailReceipt(detailOld.getId(), model);
		}
		//kiểm tra số lượng có dủ hay không
		if ((quantityProduct + quantityOld - quantityNew) >= 0) {
			detailReceiptService.update(detailReceipt);
			int idReceipt =  detailReceipt.getReceipt().getId();
			Receipt receipt = receiptService.findById(idReceipt);
			model.addAttribute("receipt", receipt);
			return "receipt/receipt-view";
		} else {
			model.addAttribute("errorQuantity", "Quantity product not enough");
			model.addAttribute("detailReceipt", detailReceipt);
			return "/receipt/detailReceipt-update";
		}
	}
	
	@RequestMapping(value = {"receipt/detailReceiptDelete/{id}"})
	public Object doDeleteDetailReceipt(@PathVariable("id") int id, Model model){
		//
		DetailReceipt detailReceipt = detailReceiptService.findById(id);
		Receipt receipt = detailReceipt.getReceipt();
		detailReceiptService.delete(detailReceipt);
		//nếu trong receipt có 1 detail này thì receipt này sẽ bị xóa -> quay lại trang listReceipt
		if(receipt.getDetailReceipts().size() == 1) {
			List<Receipt> listReceipt = receiptService.findAll();
			model.addAttribute("listReceipt", listReceipt);
			return "receipt/receipt-list";
		} else {
			receipt = receiptService.findById(receipt.getId());
			model.addAttribute("receipt_id", receipt.getId());
			model.addAttribute("receipt", receipt);
			return "receipt/receipt-view";
		}
	}
}



