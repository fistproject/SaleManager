package me.project.customermanagement.controller;


import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import me.project.customermanagement.entities.Category;
import me.project.customermanagement.entities.Product;
import me.project.customermanagement.service.CategoryService;
import me.project.customermanagement.service.ProductService;
import me.project.customermanagement.utils.ValidatorProduct;

@Controller
public class ProductController {

	@Autowired
	private ProductService productService;
	@Autowired
	private CategoryService categoryService;
	
	@RequestMapping(value = {"/product/","/product/product-list"})
	public String listProduct(Model model) {
		List<Product> products = productService.findAll();
		model.addAttribute("listProduct", products);
		return "product/product-list";
	}
	
	@RequestMapping(value = {"product/product-save"})
	public String insertProduct(Model model) {
		List<Category> categories = categoryService.findAll();
		model.addAttribute("categories", categories);
		model.addAttribute("product", new Product());
		return "product/product-save";
		//return "product-save";
	}
	
	@RequestMapping("product/product-view/{id}")
	public String viewProduct(@PathVariable int id, Model model)throws Exception  {
		Product product = productService.findById(id);
		if (product.getIsValid()) {
			model.addAttribute("product", product);
			return "product/product-view";
		}else {
			model.addAttribute("errors", "không tồn tại product!");
			model.addAttribute("listProduct", productService.findAll());
			return "redirect:product/product-list";
		}
	}
	
	@RequestMapping(value = {"product/saveProduct"}, method = RequestMethod.POST)
	public Object doSaveProduct(@RequestParam("category.id") String id, @RequestParam("price") String price, @ModelAttribute("product") @Valid  Product product, BindingResult theBindingResult,  Model model){

		if (!ValidatorProduct.checkPrice(price)) {
			model.addAttribute("errorPrice", "giá tiền không hợp lệ!");
			model.addAttribute("product", product);
			return "product/product-save";
		}
		Category category = categoryService.findById(Integer.parseInt(id));
		product.setCategory(category);
		if (theBindingResult.hasErrors()) {
			List<Category> categories = categoryService.findAll();
			model.addAttribute("categories", categories);
			return "product/product-save";
		}
		
		productService.save(product);
		model.addAttribute("listProduct", productService.findAll());
		return "redirect:product-list";
	}
	
	@RequestMapping("product/product-update/{id}")
	public String updateProduct(@PathVariable int id, Model model) {
		List<Category> categories = categoryService.findAll();
		model.addAttribute("categories", categories);
		Product product = productService.findById(id);
		model.addAttribute("product", product);
		return "product/product-update";
	  }
	
	@RequestMapping("product/updateProduct")
	public String doUpdateProduct(@RequestParam("price") String price, @RequestParam("category.id") String id,
			@ModelAttribute("product") @Valid Product product, BindingResult theBindingResult, Model model)throws Exception {
		
		if (!ValidatorProduct.checkPrice(price)) {
			model.addAttribute("errorPrice", "giá tiền không hợp lệ!");
			model.addAttribute("product", product);
			return "product/product-update";
		}
		Category category = categoryService.findById(Integer.parseInt(id));
		product.setCategory(category);
		if (theBindingResult.hasErrors()) {
			List<Category> categories = categoryService.findAll();
			model.addAttribute("categories", categories);
			return "product/product-update";
		}
		
		productService.update(product);
		model.addAttribute("listProduct", productService.findAll());
		return "redirect:product-list";
	}
	
	@RequestMapping("product/productDelete/{id}")
	public String doDelete(@PathVariable int id, Model model) {
		
		Product product = productService.findById(id);
		if (product != null) {
			productService.delete(product);
			List<Product> products = productService.findAll();
			model.addAttribute("listProduct", products);
			return "redirect:/product/product-list";
		} else {
			List<Product> products = productService.findAll();
			model.addAttribute("listProduct", products);
			model.addAttribute("errors", "Không tồn tại product!");
			return "/product/product-list";
		}
	}
	
}
