package me.project.customermanagement.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import me.project.customermanagement.entities.Customer;
import me.project.customermanagement.entities.DetailReceipt;
import me.project.customermanagement.entities.Receipt;
import me.project.customermanagement.service.CustomerService;
import me.project.customermanagement.service.DetailReceiptService;
import me.project.customermanagement.service.ReceiptService;
import me.project.customermanagement.utils.Validator;

@Controller
public class ReceiptController {

	@Autowired
	private ReceiptService receiptService;
	
	@Autowired
	private CustomerService customerService;
	
	@Autowired
	private DetailReceiptService detailReceiptService;
	
	@RequestMapping(value = {"/receipt","/receipt/receipt-list"})
	public String listReceipt(Model model) {
		List<Receipt> receipts = receiptService.findAll();
		model.addAttribute("listReceipt", receipts);
		return "receipt/receipt-list";
	}
	
	@RequestMapping("receipt/receipt-view/{id}")
	public String viewProduct(@PathVariable int id, Model model)throws Exception  {
		Receipt receipt = receiptService.findById(id);
		receipt.getCustomer().getId();
		List<DetailReceipt>  detailReceipts = receipt.getDetailReceipts();
		for (DetailReceipt dr : detailReceipts) {
			dr.getProduct().getId();
		}
		model.addAttribute("receipt", receipt);
		return "receipt/receipt-view";
	}
	
	@RequestMapping(value = {"receipt/receipt-save"})
	public String insertReceipt( Model model) {
		model.addAttribute("detailReceipt", new DetailReceipt());
		model.addAttribute("receipt", new Receipt());
		return "receipt/receipt-save";
	}
	
	@RequestMapping(value = {"receipt/receiptDelete/{id}"})
	public Object doDeleteReceipt(@PathVariable("id") int id, Model model){
		//
		Receipt receipt = receiptService.findById(id);
		List<DetailReceipt> listDetailReceipt = receipt.getDetailReceipts();
		int numberOfReceipt = listDetailReceipt.size();
		while (numberOfReceipt > 0) {
			detailReceiptService.delete(listDetailReceipt.get(numberOfReceipt - 1));
			numberOfReceipt -= 1;
		}
		List<Receipt> receipts = receiptService.findAll();
		model.addAttribute("listReceipt", receipts);
		return "receipt/receipt-list";
	}
	
	@RequestMapping(value = {"receipt/saveReceipt"}, method = RequestMethod.POST)
	public Object doSaveReceipt(@RequestParam("EmailOrPhone") String EmailOrPhone, @ModelAttribute("receipt") @Valid  Receipt receipt, BindingResult theBindingResult,  Model model){
		
		int id = receipt.getCustomer().getId();
		Customer customerByID = customerService.findById(id);
		if (theBindingResult.hasErrors()) {
			model.addAttribute("errorID", "ID not Null");
			if (EmailOrPhone == "") {
				model.addAttribute("errorEmailOrPhone", "Email or Phone not null");
			}
			return "/receipt/receipt-save";
		}

		if (customerByID == null) {
			model.addAttribute("errorID", "ID not exist");
			return "receipt/receipt-save";
		}
		
		if (EmailOrPhone == "") {
			model.addAttribute("errorEmailOrPhone", "Email or Phone not null");
			return "/receipt/receipt-save";
		}
		// check khong dung chuan email hoac sdt
					//khong phai email va khong phai phone
		if ((!Validator.checkEmail(EmailOrPhone)) && (!Validator.checkPhone(EmailOrPhone))) {
			model.addAttribute("errorEmailOrPhone", "Email or Phone not true");
			return "/receipt/receipt-save";
		}
		List<Customer> listCustomerByEmailOrPhone = new ArrayList<Customer>();
		//neu la email thi kiem tra ton tai email nay khong
		if (Validator.checkEmail(EmailOrPhone)) {
			listCustomerByEmailOrPhone = customerService.findByEmail(EmailOrPhone);
		}
		//neu la phone thi kiem tra ton tai phone nay khong
		if (Validator.checkPhone(EmailOrPhone)) {
			listCustomerByEmailOrPhone = customerService.findByPhone(EmailOrPhone);
		}
		if (listCustomerByEmailOrPhone.size() == 0) {
			model.addAttribute("errorEmailOrPhone", "Email or Phone not exist");
			return "receipt/receipt-save";
		}	
		int idOfListCustomerByPhone = listCustomerByEmailOrPhone.get(0).getId();
		if (id != idOfListCustomerByPhone) {
			model.addAttribute("errorNotTrue", "ID or Email/Phone not true");
			return "receipt/receipt-save"; 
		}	
		receipt.setCustomer(listCustomerByEmailOrPhone.get(0));
		receipt.setTotalQuantity(0);
		receiptService.save(receipt);
		model.addAttribute("id", receipt.getId());
		return "receipt/receipt-view";
		
	}
	
}
