package me.project.customermanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
@SpringBootApplication
@ComponentScan({ "me.project.customermanagement" })
public class SpringBootHibernateSessionApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootHibernateSessionApplication.class, args);
	}

}
