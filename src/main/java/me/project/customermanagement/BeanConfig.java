package me.project.customermanagement;

import java.util.Properties;

import javax.naming.NamingException;
import javax.sql.DataSource;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.orm.jpa.JpaVendorAdapter;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

@Configuration
//@EnableJpaRepositories(entityManagerFactoryRef = "entityManagerFactory",transactionManagerRef = "transactionManager")
//@EnableTransactionManagement
public class BeanConfig {
	 
	@Autowired
	private Environment env;//dai dien cho application.propertiers
	
	@Bean(name = "dataSource")
	public DataSource getDataSource() {
		//lay các giá trị của database trong file application.properties(url; user, pass) gán cho datasource
		DriverManagerDataSource dataSource = new DriverManagerDataSource();
		dataSource.setDriverClassName(env.getProperty("spring.datasource.driver-class-name"));
		dataSource.setUrl(env.getProperty("spring.datasource.url"));
		dataSource.setUsername(env.getProperty("spring.datasource.username"));
		dataSource.setPassword(env.getProperty("spring.datasource.password"));
		return dataSource;
		
	}

	@Autowired
	@Bean(name = "sessionFactory")
	public SessionFactory getSessionFactory(DataSource dataSource) throws Exception {
		//gán các giá trị của Hibernate trong file application.properties
	    Properties properties = new Properties();
	    properties.put("hibernate.dialect", env.getProperty("spring.jpa.properties.hibernate.dialect"));
	    properties.put("hibernate.show_sql", env.getProperty("spring.jpa.show-sql"));
	    //properties.put("hibernate.show_sql", true);
	    properties.put("current_session_context_class", //
	        env.getProperty("spring.jpa.properties.hibernate.current_session_context_class"));
	    
	    //set các giá trị cho factoryBean là properties, datasource
	    LocalSessionFactoryBean factoryBean = new LocalSessionFactoryBean();
	    
	    // Package contain entity classes
	    factoryBean.setPackagesToScan("me.project.customermanagement.entities");
	    factoryBean.setDataSource(dataSource);
	    factoryBean.setHibernateProperties(properties);
	    factoryBean.afterPropertiesSet();
	    
	    //
	    SessionFactory sf = factoryBean.getObject();
	    System.out.println("## getSessionFactory: " + sf);
	    return sf;
	 }
	  
	@Autowired
	@Bean(name = "transactionManager")
	public HibernateTransactionManager getTransactionManager(SessionFactory sessionFactory) {
	    HibernateTransactionManager transactionManager = new HibernateTransactionManager(sessionFactory);
	    return transactionManager;
	}
	  
//	  @Bean(name="entityManagerFactory")
//	  public EntityManager entityManager() {
//	      return EntityManagerFactory().getObject().createEntityManager();
//	  }


	private Properties jpaProperties() {
		Properties properties = new Properties();
		properties.put("hibernate.dialect",
				env.getRequiredProperty("spring.jpa.properties.hibernate.dialect"));
		return properties;
	}
	  
	@Autowired
	@Bean
	public JpaVendorAdapter jpaVendorAdapter() {
		HibernateJpaVendorAdapter hibernateJpaVendorAdapter = new HibernateJpaVendorAdapter();
		return hibernateJpaVendorAdapter;
	}
	  
	@Autowired
	@Bean(name = "entityManagerFactory")
	@Primary
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws NamingException {
		LocalContainerEntityManagerFactoryBean factoryBean = new LocalContainerEntityManagerFactoryBean();
	    factoryBean.setDataSource(getDataSource());
	    factoryBean.setPackagesToScan("me.project.customermanagement.entities");
	    factoryBean.setJpaVendorAdapter(jpaVendorAdapter());
	    factoryBean.setJpaProperties(jpaProperties());
	    return factoryBean;
	}
	  
}
