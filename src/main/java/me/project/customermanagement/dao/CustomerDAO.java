package me.project.customermanagement.dao;

import java.util.List;

import me.project.customermanagement.entities.Customer;

public interface CustomerDAO {

	public void save(final Customer customer);
	public void update(final Customer customer);
	public Customer findById(final int id);
	public List<Customer> findByEmail(final String email) ;
	public List<Customer> findByEmailNotHaveId(final String email, final int id);
	public List<Customer> findByPhoneNotHaveId(final String phone, final int id);
	public List<Customer> findByPhone(final String phone);
	public void delete(final Customer customer);
	public List<Customer> findAll() ;
}
