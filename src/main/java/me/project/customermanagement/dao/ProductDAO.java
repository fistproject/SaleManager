package me.project.customermanagement.dao;

import java.util.List;

import me.project.customermanagement.entities.Product;

public interface ProductDAO {

	public void save(final Product product);
	public void update(final Product product);
	public Product findById(final int id);
	public void delete(final Product product);
	public List<Product> findAll();
}
