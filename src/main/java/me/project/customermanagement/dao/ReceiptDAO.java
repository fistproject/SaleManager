package me.project.customermanagement.dao;

import java.util.List;

import me.project.customermanagement.entities.Receipt;

public interface ReceiptDAO {

	public Receipt findById(final int id) ;
	public List<Receipt> findAll();
}
