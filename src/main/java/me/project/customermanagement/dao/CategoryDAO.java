package me.project.customermanagement.dao;

import java.util.List;

import me.project.customermanagement.entities.Category;

public interface CategoryDAO {
	
	public void save(final Category category);
	public void update(final Category category);
	public Category findById(final int id) ;
	public void delete(final Category category);
	public List<Category> findAll();

}
