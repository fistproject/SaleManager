package me.project.customermanagement.dao;

import java.util.List;

import me.project.customermanagement.entities.DetailReceipt;

public interface DetailReceiptDAO {

	public void update(final DetailReceipt detailReceipt) ;
	public void save(final DetailReceipt detailReceipt);
	public DetailReceipt findById(final int id) ;
	public List<DetailReceipt> findAll();
	public List<DetailReceipt> findListDetailByReceiptId(final int DetailReceiptId);
	public void delete(final DetailReceipt detailReceipt);
}
