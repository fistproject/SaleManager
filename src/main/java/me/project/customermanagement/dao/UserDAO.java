package me.project.customermanagement.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import me.project.customermanagement.entities.User;
@Repository
public interface UserDAO extends CrudRepository<User, Integer> {

    User findByEmail(String email);

}