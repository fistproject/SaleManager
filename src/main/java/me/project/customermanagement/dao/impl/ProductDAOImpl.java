package me.project.customermanagement.dao.impl;



import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import me.project.customermanagement.dao.ProductDAO;
import me.project.customermanagement.entities.Product;

@Repository(value = "productDAO")
@Transactional(rollbackFor = Exception.class)
public class ProductDAOImpl  implements ProductDAO{
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public void save(final Product product) {
		product.setValid(true);
		Session session = this.sessionFactory.getCurrentSession();
		session.save(product);
	}
	
	public void update(final Product product) {
		product.setValid(true);
		Session session = this.sessionFactory.getCurrentSession();
		session.update(product);
		
	}
	
	public Product findById(final int id) {
		Session session = this.sessionFactory.getCurrentSession();
		return session.get(Product.class, id);
	}
	
	public void delete(final Product product) {
		product.setValid(false);
		Session session = this.sessionFactory.getCurrentSession();
		session.update(product);
	}
	
	public List<Product> findAll(){
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("FROM Product as p where p.isValid = true ORDER BY p.id ", Product.class).getResultList();
		
	}

}
