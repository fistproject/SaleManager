package me.project.customermanagement.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import me.project.customermanagement.dao.DetailReceiptDAO;
import me.project.customermanagement.entities.Customer;
import me.project.customermanagement.entities.DetailReceipt;

@Repository(value = "DetailReceiptDAO")
@Transactional(rollbackFor = Exception.class)
public class DetailReceiptDAOImpl implements DetailReceiptDAO {
	
	@Autowired
	private SessionFactory sessionFactory;

	public List<DetailReceipt> findListDetailByReceiptId(final int DetailReceiptId) {
		Session session = this.sessionFactory.getCurrentSession();
		String hql = "FROM DetailReceipt as d WHERE d.receipt.id = :detailReceiptId";
		  Query query = session.createQuery(hql);
		  query.setParameter("detailReceiptId", DetailReceiptId);
		  List results = query.list();
		return results; 
	}
	
	public List<DetailReceipt> findAll(){
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("FROM Receipt as r ORDER BY r.id ", DetailReceipt.class).getResultList();	
	}
	
	public DetailReceipt findById(final int id) {
		  Session session = this.sessionFactory.getCurrentSession();
		  return session.get(DetailReceipt.class, id);
	  }
	
	public void save(final DetailReceipt detailReceipt) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(detailReceipt);
	}
	
	public void update(final DetailReceipt detailReceipt) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(detailReceipt);
	}
	
	public void delete(final DetailReceipt detailReceipt) {
		Session session = this.sessionFactory.getCurrentSession();
		session.delete(detailReceipt);
		
	}
}
