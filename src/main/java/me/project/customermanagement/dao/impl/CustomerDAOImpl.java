package me.project.customermanagement.dao.impl;


import java.util.List;


import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import me.project.customermanagement.dao.CustomerDAO;
import me.project.customermanagement.entities.Customer;

@Repository(value = "customerDAO")
@Transactional(rollbackFor = Exception.class)
public class CustomerDAOImpl implements CustomerDAO {

	  @Autowired
	  private SessionFactory sessionFactory;
	  
	  public void save(final Customer customer) {
		  Session session = this.sessionFactory.getCurrentSession();
		  customer.setIsValid(true);
		  session.save(customer);
	  }
	  
	  public void update(final Customer customer) {
		  Session session = this.sessionFactory.getCurrentSession();
		  customer.setIsValid(true);
		  session.update(customer);
	  }
	  public Customer findById(final int id) {
		  Session session = this.sessionFactory.getCurrentSession();
		  return session.get(Customer.class, id);
	  }
	  
	  public List<Customer> findByEmail(final String email) {
		  Session session = this.sessionFactory.getCurrentSession();
		  String hql = "FROM Customer as c WHERE c.email = :customer_email";
		  Query query = session.createQuery(hql);
		  query.setParameter("customer_email", email);
		  List results = query.list();
		  return results;
	  }
	  
	  public List<Customer> findByEmailNotHaveId(final String email, final int id) {
		  Session session = this.sessionFactory.getCurrentSession();
		  String hql = "FROM Customer as c WHERE c.email = :customer_email and c.id != :customer_id";
		  Query query = session.createQuery(hql);
		  query.setParameter("customer_email", email);
		  query.setParameter("customer_id", id);
		  List results = query.list();
		  return results;
	  }
	  public List<Customer> findByPhoneNotHaveId(final String phone, final int id) {
		  Session session = this.sessionFactory.getCurrentSession();
		  String hql = "FROM Customer as c WHERE c.phone = :customer_phone and c.id != :customer_id";
		  Query query = session.createQuery(hql);
		  query.setParameter("customer_phone", phone);
		  query.setParameter("customer_id", id);
		  List results = query.list();
		  return results;
	  }
	  
	  public List<Customer> findByPhone(final String phone) {
		  Session session = this.sessionFactory.getCurrentSession();
		  String hql = "FROM Customer as c WHERE c.phone = :customer_phone";
		  Query query = session.createQuery(hql);
		  query.setParameter("customer_phone", phone);
		  List results = query.list();
		  return results;
	  }
	  
	  public void delete(final Customer customer) {
		  customer.setIsValid(false);
		  Session session = this.sessionFactory.getCurrentSession();
		  session.update(customer);
	  }
	  
	  public List<Customer> findAll() {
		  Session session = this.sessionFactory.getCurrentSession();
		  return session.createQuery("FROM Customer as c WHERE c.isValid = true ORDER BY c.id asc", Customer.class).getResultList();
	  }
}
