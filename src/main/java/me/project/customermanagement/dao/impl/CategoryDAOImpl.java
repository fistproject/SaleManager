package me.project.customermanagement.dao.impl;


import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import me.project.customermanagement.dao.CategoryDAO;
import me.project.customermanagement.entities.Category;

@Repository(value = "CategoryDAO")
@Transactional(rollbackFor = Exception.class)
public class CategoryDAOImpl implements CategoryDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	public void save(final Category category) {
		category.setValid(true);
		Session session = this.sessionFactory.getCurrentSession();
		session.save(category);
	}
	
	public void update(final Category category) {
		Session session = this.sessionFactory.getCurrentSession();
		session.update(category);	
	}
	
	
	public Category findById(final int id) {
		Session session = this.sessionFactory.getCurrentSession();
		return session.get(Category.class, id);
	}
	
	public void delete(final Category category) {
		category.setValid(false);
		Session session = this.sessionFactory.getCurrentSession();
		session.update(category);
	}
	
	public List<Category> findAll(){
		Session session = this.sessionFactory.getCurrentSession();
		List<Category> categories = session.createQuery("from Category", Category.class).getResultList();
		return categories;
	}
	
	public List<Category> findTopSupperCategory(){
		Session session = this.sessionFactory.getCurrentSession();
		List<Category> categories = session.createQuery("from Category where supperCategory = null", Category.class).getResultList();
		return categories;
	}
	
	public List<Category> findListSubCategoryOfCategory(Category category){
		Session session = this.sessionFactory.getCurrentSession();
		String hql = "FROM Category as c WHERE c.supperCategory = :category";
		  Query query = session.createQuery(hql);
		  query.setParameter("category", category);
		  List results = query.list();
		  return results;
	}


	
}
