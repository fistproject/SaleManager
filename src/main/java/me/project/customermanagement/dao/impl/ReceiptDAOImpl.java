package me.project.customermanagement.dao.impl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import me.project.customermanagement.dao.ReceiptDAO;
import me.project.customermanagement.entities.DetailReceipt;
import me.project.customermanagement.entities.Receipt;

@Repository(value = "receiptDAO")
@Transactional(rollbackFor = Exception.class)
public class ReceiptDAOImpl implements ReceiptDAO{

	@Autowired
	private SessionFactory sessionFactory;
	
//	public void save(final Product product) {
//		product.setValid(true);
//		Session session = this.sessionFactory.getCurrentSession();
//		session.save(product);
//	}
	
	public Receipt findById(final int id) {
		Session session = this.sessionFactory.getCurrentSession();
		Receipt result = session.get(Receipt.class, id);
		return session.get(Receipt.class, id);
	}
	
	public List<DetailReceipt> findListDetailByReceiptId(final int DetailReceiptId) {
		Session session = this.sessionFactory.getCurrentSession();
		String hql = "FROM DetailReceipt as d WHERE d.receipt.id = :detailReceiptId";
		  Query query = session.createQuery(hql);
		  query.setParameter("detailReceiptId", DetailReceiptId);
		  List results = query.list();
		return results; 
	}
	
	public List<Receipt> findAll(){
		Session session = this.sessionFactory.getCurrentSession();
		return session.createQuery("FROM Receipt as r ORDER BY r.id ", Receipt.class).getResultList();	
	}
	public void save(final Receipt receipt) {
		Session session = this.sessionFactory.getCurrentSession();
		session.save(receipt);
	}
}
