package me.project.customermanagement.dao;

import org.springframework.data.repository.CrudRepository;

import me.project.customermanagement.entities.Role;

public interface RoleDAO extends CrudRepository<Role, Integer> {

    Role findByName(String name);

}
