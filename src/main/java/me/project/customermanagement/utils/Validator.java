 package me.project.customermanagement.utils;

import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Validator {
	public static boolean checkEmail(String email) {
		final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
		            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
		Pattern pattern = Pattern.compile(EMAIL_PATTERN);
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	  }
	public static String convertLongToString(long unixTime) {
		new java.util.Date((long)unixTime);
		java.util.Date time = new java.util.Date((long)unixTime*1000);
		Format f = new SimpleDateFormat("yyyy-MM-dd");
		String strDate = f.format(time);
		//System.out.println(strDate);
		return strDate;
	  }
	
	 //check phone
	public static Boolean checkPhone(String phone) {
		Boolean ret = true;
		ret = phone.matches("^[0-9]+$");
		return ret;
	}
	//convert long to Date
	public static Date convertLongToDate(long unixTime) {
		new java.util.Date((long)unixTime);
		java.util.Date time = new java.util.Date((long)unixTime*1000);
		return time;
	  }
	  //convert string to long
	public static long convertStringToLong(String date) {
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			Date dt = sdf.parse(date);
			long epoch = dt.getTime()/1000;
			return epoch;
			} catch(ParseException e) {
				return -1;
			}
	  }
}
