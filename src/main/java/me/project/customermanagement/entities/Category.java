package me.project.customermanagement.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

@Entity
@Table(name = "category")
public class Category {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@Column(name = "categoryName")
	@NotBlank(message = "name not null")
	@Size(max = 20)
	private String category_name;
	
	@OneToMany(mappedBy = "category",
			cascade = CascadeType.ALL,
			fetch=FetchType.EAGER)
	private List<Product> products = new ArrayList<>();

	@OneToMany(mappedBy = "supperCategory")
	@LazyCollection(LazyCollectionOption.FALSE)
	private List<Category> categories = new ArrayList<>();
	
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "supperCategoryID")
	private Category supperCategory;
	
	@Column(name = "isValid")
	private boolean isValid;
	
	@Column(name = "enable")
	private boolean enable;
	
	@Column(name = "description")
	@Size(max = 100)
	private String description;
	
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCategory_name() {
		return category_name;
	}

	public void setCategory_name(String category_name) {
		this.category_name = category_name;
	}

	public List<Product> getProducts() {
		return products;
	}
	
	public List<Category> getCategories() {
		return categories;
	}

	public void setCategories(List<Category> categories) {
		this.categories = categories;
	}

	public Category getSupperCategory() {
		return supperCategory;
	}

	public void setSupperCategory(Category supperCategory) {
		this.supperCategory = supperCategory;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public boolean getIsValid() {
		return isValid;
	}
	
	public void setValid(boolean isValid) {
		this.isValid = isValid;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}
	
}
