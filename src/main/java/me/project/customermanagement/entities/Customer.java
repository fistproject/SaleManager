package me.project.customermanagement.entities;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import me.project.customermanagement.utils.Validator;


@Entity
@Table(name = "customer")
public class Customer {

	 @Id
	  @Column(name = "id")
	  @GeneratedValue(strategy = GenerationType.IDENTITY)
	  private int id;
	  
	  @Column(name = "name")
	  @NotBlank(message="name not null")
	  private String name;
	  
	  @Column(name = "address")
	  private String address;
	  
//	  dob
	  @Column(name = "dob")
	  private long dob;
	  
//	  email
	  @Column(name = "email")
	  @NotBlank(message = "email not null")
	  private String email;
	  
//	  phone
	  @Column(name = "phone")
	  @NotBlank(message = "phone not null")
	  private String phone;
	  
//	  gender
	  @Column(name = "gender")
	  private String gender;
	  
//	  isValid
	  @Column(name = "isValid")
	  private Boolean isValid;
	   
	  @OneToMany(
		        mappedBy = "customer",
		        cascade = CascadeType.ALL,
		        orphanRemoval = true
		    )
	  private List<Receipt> receipts = new ArrayList<>();
	  
	  public List<Receipt> getReceipts() {
		return receipts;
	}
	public void setReceipts(List<Receipt> receipts) {
		this.receipts = receipts;
	}
	
	public void setIsValid(Boolean isValid) {
		this.isValid = isValid;
	}
	// getter - setter
	  public int getId() {
		  return id;
	  }
	  public void setId(int id) {
		  this.id = id;
	  }
	  public String getName() {
		  return name;
	  }
	  public void setName(String name) {
		  this.name = name;
	  }
	  public String getAddress() {
		  return address;
	  }
	  public void setAddress(String address) {
		  this.address = address;
	  }
	  public long getDob() {
		  return dob;
	  }
	  public String getDOB() {
		  return Validator.convertLongToString(this.dob);
	  }
	  public void setDob(long dob) {
		  this.dob = dob;
	  }
	  public String getEmail() {
		  return email;
	  }
	  public void setEmail(String email) {
		  this.email = email;
	  }
	  public String getPhone() {
		  return phone;
	  }
	  public void setPhone(String phone) {
		  this.phone = phone;
	  }
	  public String getGender() {
		  return gender;
	  }
	  public void setGender(String gender) {
		  this.gender = gender;
	  }
	  public Boolean getIsValid() {
		  return isValid;
	  }
	  public void setIsValid(boolean b) {
		  this.isValid = b;
	  }
  
}
