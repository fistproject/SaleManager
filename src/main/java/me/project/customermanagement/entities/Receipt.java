package me.project.customermanagement.entities;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "receipt")
public class Receipt {

	@Id
	@Column(name = "id")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
//	@Column(name = "idCustomer")
//	private long idCustomer;
	
	@Column(name = "TotalQuantity")
	private long TotalQuantity;
	
	@ManyToOne//(fetch = FetchType.LAZY)
	@JoinColumn(name = "idCustomer")
    private Customer customer;

	@OneToMany(mappedBy = "receipt",
			cascade = CascadeType.ALL,
			fetch=FetchType.EAGER)
	private List<DetailReceipt> detailReceipts = new ArrayList<>();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public long getTotalQuantity() {
		return TotalQuantity;
	}

	public void setTotalQuantity(long totalQuantity) {
		TotalQuantity = totalQuantity;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public List<DetailReceipt> getDetailReceipts() {
		return detailReceipts;
	}

	public void setDetailReceipts(List<DetailReceipt> detailReceipts) {
		this.detailReceipts = detailReceipts;
	}

	
}
